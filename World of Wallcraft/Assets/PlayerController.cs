﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Analytics;

[NetworkSettings(channel = 1)]
public class PlayerController : NetworkBehaviour
{
    private float rotationX = 0.0f;
    private float rotationY = 0.0f;
    private float sensitivityX = 8.0f;
    private float sensitivityY = 8.0f;
    private float minimumX = -360.0f;
    private float maximumX = 360.0f;
    private float minimumY = -60.0f;
    private float maximumY = 60.0f;

    private float maxVelocityChange = 10.0f;
    private float speed = 10.0f;

    public GameObject m_shotPrefab;
    public Transform m_muzzle;
    
    public override void OnStartLocalPlayer()
    {
        //set the scene cam to local player
        Camera.main.transform.SetParent(gameObject.transform);
        Camera.main.transform.position = new Vector3(0, 0, 0);
        Camera.main.transform.rotation = Quaternion.identity;

        //The OnStartLocalPlayer function is a good place to do initialization that is only for the local player, such as configuring cameras and input.
        GetComponent<MeshRenderer>().material.color = Color.blue;

        Cursor.visible = false;

        // Reference the Unity Analytics namespace

        Analytics.CustomEvent("localStart");
        

    }

    void Update()
    {
        // with this check only the local player can execute the movement
        if (!isLocalPlayer)
        {
            return;
        }

        // move
        rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        transform.rotation = Quaternion.Euler(new Vector3(0, rotationX, 0));
        Camera.main.transform.rotation = Quaternion.Euler(new Vector3(-rotationY, rotationX, 0));

        //Camera.main.transform.position = transform.position;

        // Calculate how fast we should be moving
        Vector3 _targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        _targetVelocity = transform.TransformDirection(_targetVelocity);
        _targetVelocity *= speed;

        // Apply a force that attempts to reach our target velocity
        Vector3 _velocity = GetComponent<Rigidbody>().velocity;
        Vector3 _velocityChange = (_targetVelocity - _velocity);
        _velocityChange.x = Mathf.Clamp(_velocityChange.x, -maxVelocityChange, maxVelocityChange);
        _velocityChange.z = Mathf.Clamp(_velocityChange.z, -maxVelocityChange, maxVelocityChange);
        _velocityChange.y = 0;
        GetComponent<Rigidbody>().AddForce(_velocityChange, ForceMode.VelocityChange);

        //LMB
        if (Input.GetMouseButton(0))
        {
            CmdFire();
        }
    }

    [Command] // This [Command] code is called on the Client, but it is run on the Server!
    void CmdFire()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        // Create the Bullet from the Bullet Prefab
        GameObject bullet = (GameObject)Instantiate(
            m_shotPrefab,
            m_muzzle.position,
            m_muzzle.rotation);

        // Add velocity to the bullet
        //bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        // Spawn the bullet on the Clients
        NetworkServer.SpawnWithClientAuthority(bullet, connectionToClient);
        //network.serverspawn!!

        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }
}