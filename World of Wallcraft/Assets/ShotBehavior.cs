﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

[NetworkSettings(channel = 1)]
public class ShotBehavior : NetworkBehaviour {

    public float speedFactor = 100.0f;
    private float spawnPos;
    private bool reached = false;
    public GameObject m_wall;
    private GameObject m_PC; // ref to script on player
    private float dist;

    // Use this for initialization
    void Start () {


        m_PC = GameObject.FindGameObjectWithTag("Player");

    }

    private void Update()
    {
        // if (Input.GetKey(KeyCode.Q)) { dist += 5; }
        // if (Input.GetKey(KeyCode.E)) { dist -= 5; }
        float dist = Vector3.Distance(m_PC.GetComponent<PlayerController>().transform.position, transform.position);

        if (dist > 50)
        {
            reached = true;
        }


        if (reached == true)
        {
            Cmdspawnwall();
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
		transform.position += transform.forward * Time.deltaTime * speedFactor;

	}

    [Command]
    void Cmdspawnwall()
    {
        GameObject go = GameObject.Instantiate(m_wall, transform.position, Quaternion.identity) as GameObject;
        //NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
        Destroy(gameObject, 0.01f);
    }
}
