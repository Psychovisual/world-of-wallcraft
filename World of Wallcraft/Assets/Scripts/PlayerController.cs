﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private float rotationX = 0.0f;
    private float rotationY = 0.0f;
    private float sensitivityX = 8.0f;
    private float sensitivityY = 8.0f;
    private float minimumX = -360.0f;
    private float maximumX = 360.0f;
    private float minimumY = -60.0f;
    private float maximumY = 60.0f;

    private float maxVelocityChange = 10.0f;
    private float speed = 10.0f;
    private float jumpHeight = 14.0f;

    public float spawnDist = 25.0f;
    public float spawnMin = 2.0f;
    public float spawnMax = 200.0f;

    public RawImage highlighter1;
    public RawImage highlighter2;
    public Text spawnDistText;

    public GameObject m_shotPrefab;
    public GameObject wall1;
    public GameObject wall2;

    public GameObject s_wall;
    //public Transform m_muzzle;
    
    public void Start()
    {
        Cursor.visible = false;
      
        GetComponent<MeshRenderer>().material.color = Color.blue;

        Cursor.visible = false;
        s_wall = wall1;
        highlighter1.enabled = true;
        highlighter2.enabled = false;
    }
    public void FixedUpdate()
    {
        // move
        rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

        transform.rotation = Quaternion.Euler(new Vector3(0, rotationX, 0));
        GetComponentInChildren<Camera>().transform.rotation = Quaternion.Euler(new Vector3(-rotationY, rotationX, 0));

        // Calculate how fast we should be moving
        Vector3 _targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        _targetVelocity = transform.TransformDirection(_targetVelocity);
        _targetVelocity *= speed;

        // Apply a force that attempts to reach our target velocity
        Vector3 _velocity = GetComponent<Rigidbody>().velocity;
        Vector3 _velocityChange = (_targetVelocity - _velocity);
        _velocityChange.x = Mathf.Clamp(_velocityChange.x, -maxVelocityChange, maxVelocityChange);
        _velocityChange.z = Mathf.Clamp(_velocityChange.z, -maxVelocityChange, maxVelocityChange);
        _velocityChange.y = 0;
        GetComponent<Rigidbody>().AddForce(_velocityChange, ForceMode.VelocityChange);
    }
    void Update()
    {
        //get scrollWheel input and clamp
        spawnDist += Input.GetAxis("Mouse ScrollWheel");
        spawnDist = Mathf.Clamp(spawnDist, spawnMin, spawnMax);

        spawnDistText.text = spawnDist.ToString();

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            s_wall = wall1;
            highlighter1.enabled = true;
            highlighter2.enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            s_wall = wall2;
            highlighter1.enabled = false;
            highlighter2.enabled = true;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            RaycastHit _rayCastHit = new RaycastHit();

            // Cast a ray downwards to see if there's anything below
            if (Physics.Raycast(transform.position, -Vector3.up, out _rayCastHit, 1.5f))
            {
                GetComponent<Rigidbody>().velocity = new Vector3(0, Mathf.Sqrt(2 * jumpHeight * 1), 0);
            }
        }

        //LMB
        if (Input.GetMouseButton(0))
        {
            CmdFire();
        }
    }

  
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        GameObject bullet = (GameObject)Instantiate(
            m_shotPrefab,
            transform.position,
            gameObject.GetComponentInChildren<Camera>().transform.rotation);

        // Add velocity to the bullet
        //bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;


        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }
}