﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class setParentCamera : NetworkBehaviour {

    private GameObject cam;

	// Use this for initialization
	void Start () {
        cam = GameObject.FindGameObjectWithTag("MainCamera");
        gameObject.transform.SetParent(cam.transform);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

/*
 * using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour
{
    public GameObject player;

    //Invoked when a button is clicked.
    public void Example(Transform newParent)
    {
        //Sets "newParent" as the new parent of the player GameObject.
        player.transform.SetParent(newParent);

        //Same as above, except this makes the player keep its local orientation rather than its global orientation.
        player.transform.SetParent(newParent, false);
    }
}
*/