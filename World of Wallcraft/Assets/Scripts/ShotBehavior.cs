﻿using UnityEngine;

public class ShotBehavior : MonoBehaviour
{

    public float speedFactor = 100.0f;
    private float spawnPos;
    private bool reached = false;    
    private float smooth = 2.0f;
    public float tiltAngle = 45.0f;
    private float a = 90;
    Vector3 LastPos;
    private float dist;


    void Start () {

        LastPos = transform.position;
        
    }

    private void Update()
    {

        dist += Vector3.Distance(transform.position, LastPos);


        if (dist > GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().spawnDist)
        {
            reached = true;
        }


        if (reached == true)
        {
            Cmdspawnwall();
        }
    }


    void FixedUpdate () {
		transform.position += transform.forward * Time.deltaTime * speedFactor;

	}

 
    void Cmdspawnwall()
    {
        float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle;
        float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
        Quaternion target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);
        GameObject go = GameObject.Instantiate
            (
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().s_wall, 
            transform.position,
            GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Camera>().transform.rotation
            ) 
            as GameObject;

        Destroy(gameObject, 0.01f);
    }
}
